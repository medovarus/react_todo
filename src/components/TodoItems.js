import React, {Component} from 'react'

class TodoItems extends Component {
    createTasks = item => {
        return (
            <li key={item.key}>
                <input
                    className="theList_text"
                    placeholder="Edit task"
                    ref={this.props.inputElement}
                    value={item.text}
                    onChange={(e) => this.props.changeInput(e, item.key)}
                ></input>
                <span className="theList_delete" onClick={() => this.props.deleteItem(item.key)}>X</span>
            </li>
        );
    };

    render() {
        const todoEntries = this.props.entries;
        const listItems = todoEntries.map(this.createTasks);

        return <ul className="theList">{listItems}</ul>
    }
}

export default TodoItems
import React, {Component} from 'react';
import './App.css';
import TodoList from './components/TodoList';
import TodoItems from './components/TodoItems';

class App extends Component {
    constructor() {
        super();
        this.state = {
            items: [],
            currentItem: {text: '', key: ''}
        }
    }

    inputElement = React.createRef();
    inputNewElement = React.createRef();

    addItem = e => {
        e.preventDefault();
        const newItem = this.state.currentItem;

        if (newItem.text !== '') {
            const items = [...this.state.items, newItem];
            this.setState({
                items: items,
                currentItem: {text: '', key: ''}
            })
        }
    };

    deleteItem = (key) => {
        const filteredItems = this.state.items.filter(item => {
            return item.key !== key;
        });

        this.setState({
            items: filteredItems
        });
    };

    changeInput = (e, key) => {
        const itemText = e.target.value;
        const items = [...this.state.items];

        items.findIndex((element, index) => {
            if (element.key === key) {
                items[index].text = itemText;
                return true;
            }
            return false;
        });

        this.setState({
            items: items
        });
    };

    handleNewInput = e => {
        const itemText = e.target.value;
        const currentItem = {text: itemText, key: Date.now()};

        this.setState({
            currentItem
        });
    };

    render() {
        return (
            <div className="App">
                <TodoItems
                    entries={this.state.items}
                    deleteItem={this.deleteItem}
                    inputElement={this.inputElement}
                    changeInput={this.changeInput}
                />
                <TodoList
                    addItem={this.addItem}
                    inputNewElement={this.inputNewElement}
                    handleNewInput={this.handleNewInput}
                    currentItem={this.state.currentItem}
                    entries={this.state.items}
                />
            </div>
        );
    }
}

export default App;
